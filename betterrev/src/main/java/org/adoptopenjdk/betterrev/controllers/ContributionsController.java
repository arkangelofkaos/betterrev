package org.adoptopenjdk.betterrev.controllers;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.adoptopenjdk.betterrev.models.Contribution;
import org.adoptopenjdk.betterrev.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequestScoped
@Path("contributions")
public class ContributionsController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ContributionsController.class); 

    @PersistenceContext
    private EntityManager entityManager;
    
    // TODO Figure out what this is and what it is used for if anything
    @Context
    private ResourceContext resourceContext;

    // TODO We will remove this temp map
    Map<Long, Contribution> mockContributions;

    // TODO - Remove this later once we have the real data from the data store
    @PostConstruct
    public void init() {
        this.mockContributions = new ConcurrentHashMap<>();
        User user = new User("karianna", "Martijn Verburg", User.OcaStatus.SIGNED);

        Contribution contribution = new Contribution("jdk9", "1", "Test Contribution", "New Contribution",
                                                     user, LocalDateTime.now(), LocalDateTime.now(), "jdk9");
        contribution.setKey(1L);
        this.mockContributions.putIfAbsent(1L, contribution);

        Contribution anotherContribution = new Contribution("jdk9", "2", "Another Test Contribution", "Contribution 2",
                                                     user, LocalDateTime.now(), LocalDateTime.now(), "jdk9");
        anotherContribution.setKey(2L);
        this.mockContributions.putIfAbsent(2L, anotherContribution);
   }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contribution> getContributions() {
      // TODO The Mock return which we'll remove later
      //return mockContributions.values();

      List<Contribution> contributions = 
              entityManager.createNamedQuery("Contribution.findAllContributions", Contribution.class)
             .getResultList();
      
      return contributions;
    }

    @Path("{contributionId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Contribution getContributionById(@PathParam("contributionId") Long contributionId) {
        // TODO get a real contribution
        //return mockContributions.get(contributionId);
        
        Contribution contribution = 
             entityManager.createNamedQuery("Contribution.findContributionById", Contribution.class)
             .setParameter("contributionId", contributionId)
             .getSingleResult();
        
        return contribution;
    }
}
