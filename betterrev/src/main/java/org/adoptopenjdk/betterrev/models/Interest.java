package org.adoptopenjdk.betterrev.models;

import java.io.Serializable;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity that represents a concept of Interest within Betterrev.
 */
@Entity
@Table(name="Interest")
@NamedQueries({
        @NamedQuery(name = "Interest.findAll", query = "SELECT i FROM Interest i"),
        @NamedQuery(name = "Interest.findInterestById",
            query = "SELECT i FROM Interest i WHERE i.id = :interestId")
})
public class Interest implements Serializable {

    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    private String filePath;

    private String project;

    public Interest() {}
    
    public Interest(String filePath, String project) {
        this.filePath = filePath;
        this.project = project;
    }

    @Override
    public String toString() {
        return "Interest [id=" + id + ", filePath=" + filePath + ", project=" + project + "]";
    }

    public boolean caresAbout(String repository, Set<String> filePaths) {
        return filePaths.stream().anyMatch((path) -> (repository.equals(project) && path.matches(this.filePath)));
    }

    public String getPath() {
        return filePath;
    }

    public String getProject() {
        return project;
    }

    public Long getId() {
        return id;
    }

    // TODO remove
    public void setKey(Long key) {
        this.id = key;
    }

}
