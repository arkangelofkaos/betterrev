package org.adoptopenjdk.betterrev.models;

/**
 * ContributionState of Contribution.
 */
public enum ContributionState {
    NULL,
    OPEN,
    PENDING_PEER_REVIEW,
    PENDING_MENTOR_APPROVAL,
    ACCEPTED,
    CLOSED,
    COMMITTED
}
