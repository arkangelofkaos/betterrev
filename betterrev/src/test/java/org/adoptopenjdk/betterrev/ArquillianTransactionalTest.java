package org.adoptopenjdk.betterrev;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;

public abstract class ArquillianTransactionalTest extends ArquillianTest {

    @PersistenceContext
    protected EntityManager entityManager;
    
    @Inject
    protected UserTransaction userTransaction;

    @Before
    public void preparePersistenceTest() throws Exception {
        startTransaction();
    }
    
    protected void startTransaction() throws Exception {
        userTransaction.begin();
        entityManager.joinTransaction();
    }

    @After
    public void commitTransaction() throws Exception {
        userTransaction.commit();
    }

}
